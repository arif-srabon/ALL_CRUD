<?php
include_once('libs/app.php');
//require "src/Model/Project.php";
//require "src/Model/Report.php";
function __autoload($classname){
    //echo $classname;exit();
    list($ns,$fn,$cn)=explode("\\",$classname);
    /*echo $cn;
    die();*/
    require "src/Model/".$cn.".php";
}
require "src/Page/title.php";
//debug($_POST);
//debug($_SESSION);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<h1>Click Here to <a href="create.html"> add</a> New Email</h1>

<table border="1">
    <tr>
        <td>SL</td>
        <td>Name</td>
        <td>Email</td>
        <td>Action</td>
    </tr>
    <?php
    if(array_key_exists('myemail',$_SESSION) && !empty($_SESSION['myemail'])){
        foreach($_SESSION['myemail'] as $key=>$value){

        ?>
    <tr>
        <td><?php echo $key+1;?></td>
        <td>
            <?php
            if(array_key_exists('fullname',$value) && !empty($value['fullname'])){
                echo $value['fullname'];
            }
            else{
                echo "No Fullname Data Availabe !";
            }
            ?>
        </td>
        <td>
            <?php
            if(array_key_exists('email',$value) && !empty($value['email'])){
                echo $value['email'];
            }
            else{
                echo "No Email Data Availabe !";
            }
            ?>
        </td>
        <td>
            <a href="show.php?id=<?php echo $key;?>">View</a> |
            <a href="edit.php?id=<?php echo $key;?>">Edit</a> |
            <a href="delete.php?id=<?php echo $key;?>">Delete</a> |
        </td>
    </tr>
    <?php
        }
    }else{
    ?>
    <tr>
        <td colspan="4">
            <h3>No Data Avaliable !!</h3>
        </td>
    </tr>
    <?php } ?>
</table>
<a href="create.html">Create</a>
</body>
</html>